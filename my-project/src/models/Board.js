export default class Board {
  constructor (line, column) {
    this.line = line
    this.column = column
    this.cells = []
  }

  addCell(line, cell) {
    this.cells[line].push(cell)

    return this
  }

  getCell(line, column) {
    return this.cells[line][column]
  }
}
