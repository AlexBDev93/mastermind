import Cell from './../models/Cell'
import Board from './../models/Board'

export default class BoardFactory {
  static create(lines, columns) {
    var board = new Board(lines, columns)
    for (var line in lines) {
      for (var i = 0; i <= columns; i++) {
        board.addCell(line, new Cell())
      }
    }
  }
}
